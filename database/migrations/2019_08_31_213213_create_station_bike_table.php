<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationBikeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_bike', function (Blueprint $table) {
            $table->unsignedBigInteger('bike_id');
            $table->unsignedBigInteger('station_id');
            $table->timestamps();
            $table->primary(array('bike_id', 'station_id'));
            $table->foreign('bike_id')->references('id')->on('bikes');
            $table->foreign('station_id')->references('id')->on('stations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_bike');
    }
}
