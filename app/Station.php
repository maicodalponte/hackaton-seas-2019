<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{

    protected $fillable = [
        'name',
        'code',
        'location',
        'bikeAmount',
        'maxBikeAmount',
    ];

    public function bikes()
    {
        return $this->belongsToMany('App\Bike', 'station_bike', 'station_id', 'bike_id')
            ->as('bikes')
            ->withPivot('created_at');
    }

}
