<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StationBike extends Model
{
    protected $table = 'station_bike';
    protected $fillable = ['bike_id', 'station_id', 'user_id'];

    public function bike()
    {
        return $this->belongsTo('App\Bike', 'bike_id');
    }

    public function station()
    {
        return $this->belongsTo('App\Station', 'station_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
