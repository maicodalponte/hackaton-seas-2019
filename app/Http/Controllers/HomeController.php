<?php

namespace App\Http\Controllers;

use App\BikeTake;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $list = BikeTake::history();
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        return view('home')->with(['history' => $list, 'token' => $tokenResult]);
    }
}
