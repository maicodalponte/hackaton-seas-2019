<?php

namespace App\Http\Controllers;

use App\Bike;
use App\Station;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function users()
    {
        $list = User::all();
        return view('users')->with(['list' => $list]);
    }

    public function stations()
    {
        $list = Station::all();
        return view('stations')->with(['list' => $list]);
    }

    public function bikes()
    {
        $list = Bike::all();
        return view('bikes')->with(['list' => $list]);
    }

}
