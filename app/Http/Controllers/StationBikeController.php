<?php

namespace App\Http\Controllers;

use App\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StationBikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $payload = $request->all();

            $station_id = $request->station_id;
            if (!$station_id) {
                throw new \Exception('Invalid Station');
            }
            $bike_id = $request->bike_id;
            if (!$bike_id) {
                throw new \Exception('Invalid Bike');
            }

            $station = Station::find($station_id);
            $result = $station->bikes()->attach($bike_id);

            return response()->json([
                "station" => $station->name,
                "bike_id" => $bike_id,
                "added" => $result,
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json([
                'message' => 'Não foi possível registrar a bike nesta estação',
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            $payload = $request->all();

            $station_id = $request->station_id;
            if (!$station_id) {
                throw new \Exception('Invalid Station');
            }
            $bike_id = $request->bike_id;
            if (!$bike_id) {
                throw new \Exception('Invalid Bike');
            }

            $station = Station::find($station_id);
            $result = $station->bikes()->detach($bike_id);

            return response()->json([
                "station" => $station->name,
                "bike_id" => $bike_id,
                "removed" => $result,
            ]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json([
                'message' => 'Não foi possível registrar a bike nesta estação',
                'error' => $e->getMessage(),
            ], 422);
        }
    }
}
