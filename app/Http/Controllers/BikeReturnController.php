<?php

namespace App\Http\Controllers;

use App\Bike;
use App\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BikeReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Bike $bike, Request $request)
    {
        try {
            $new_station = Station::find($request->station_id);
            if (!$new_station) {
                throw new \Exception('Invalid Station');
            }

            $user = $request->user();

            $station = $bike->station->first();

            $return = $bike->return()->create([
                "user_id" => $user->id,
                "station_id" => $request->station_id,
                "last_station_id" => $station ? $station->id : null,
            ]);

            if ($station && $station->id != $request->station_id) {
                $station->bikes()->detach($bike->id);
                $new_station->bikes()->attach($bike->id);
            }

            return response()->json($return);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json([
                'message' => 'Não foi possível registrar a entrega da bike',
                'error' => $e->getMessage(),
            ], 422);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
