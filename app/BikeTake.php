<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BikeTake extends Model
{
    protected $fillable = ['bike_id', 'user_id', 'station_id'];

    public function bike()
    {
        return $this->belongsTo('App\Bike');
    }

    public function station()
    {
        return $this->belongsTo('App\Station');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function history()
    {
        $results = DB::select(DB::raw("
SELECT u.name as `user`, s.name as `station`, h.*
FROM (
       SELECT 'out' as `type`, user_id, station_id, bike_id, created_at
       FROM bike_takes as bt
       UNION
       SELECT 'in' as `type`, user_id, station_id, bike_id, created_at
       FROM bike_returns as br
     ) as h
       INNER JOIN users AS u ON u.id = h.user_id
       INNER JOIN stations AS s ON s.id = h.station_id
ORDER BY created_at DESC
"));

        return $results;
    }
}
