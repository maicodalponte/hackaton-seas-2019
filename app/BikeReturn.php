<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BikeReturn extends Model
{
    protected $fillable = ['bike_id', 'user_id', 'station_id'];

    public function bike()
    {
        return $this->belongsTo('App\Bike');
    }

    public function station()
    {
        return $this->belongsTo('App\Station');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
