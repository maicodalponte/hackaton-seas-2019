<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bike extends Model
{

    protected $fillable = [
        'observation',
        'rim',
        'color',
        'source',
    ];


    public function stations()
    {
        return $this->belongsToMany('App\Station', 'station_bike', 'bike_id', 'station_id')
            ->as('stations');
    }

    public function station()
    {
        return $this->belongsToMany('App\Station', 'station_bike', 'bike_id', 'station_id')
            ->as('station')
            ->latest();
    }

    public function take()
    {
        return $this->hasMany('App\BikeTake');
    }

    public function return()
    {
        return $this->hasMany('App\BikeReturn');
    }

}
