@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-4">

            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>tipo</th>
                                <th>Nome</th>
                                <th>Estação</th>
                                <th>Bike</th>
                                <td>Horário</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($history as $item) :
                            $color = $item->type == 'in' ? 'green' : 'red';
                            ?>
                            <tr style="color: <?= $color ?>">
                                <td><?= $item->type == 'in' ? 'Entrada' : 'Saída' ?></td>
                                <td><?= $item->user ?></td>
                                <td><?= $item->station ?></td>
                                <td><?= $item->bike_id ?></td>
                                <td><?= $item->created_at ?></td>
                            </tr>
                            <?php
                            endforeach;
                            ?>
                            </tbody>
                        </table>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
