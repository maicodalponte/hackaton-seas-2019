@extends('layouts.app')

@section('content')
    <?php $users = json_encode($list)?>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <stations-list v-bind:list="{{$list}}"></stations-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
