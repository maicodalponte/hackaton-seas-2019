@extends('layouts.app')

@section('content')
    <?php $users = json_encode($list)?>

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <bikes-list v-bind:list="{{$list}}"></bikes-list>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
