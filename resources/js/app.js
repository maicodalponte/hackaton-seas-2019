/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
    'users-list',
    require('../js/components/UsersList.vue').default
);

Vue.component(
    'bikes-list',
    require('../js/components/BikesList.vue').default
);

Vue.component(
    'stations-list',
    require('../js/components/StationsList.vue').default
);

const app = new Vue({
    el: '#app',
    created() {
        console.warn('This must me awesome, right?!');
        const token = localStorage.getItem('access_token');
        if (!token) {
            axios.get('/auth/token')
                .then(function (response) {
                    const {data: {access_token}} = response;
                    localStorage.setItem('access_token', access_token);

                })
                .catch(function (error) {
                    console.error(error);
                })
                .then(function () {
                    // always executed
                });
        }
    }
});
