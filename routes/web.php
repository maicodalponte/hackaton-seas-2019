<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/auth/token', 'AuthController@token')->name('localToken');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', 'AdminController@users')->name('users');
Route::get('/stations', 'AdminController@stations')->name('stations');

Route::resource('/bikes', 'BikesController');
Route::get('/bikes', 'AdminController@bikes')->name('bikes');
